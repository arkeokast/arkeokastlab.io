---
title: "About Arkeokast"
date: 2020-06-24T11:05:59+03:00
draft: false
featured_image: ''
description: "Sometime ago, while dreaming… "
---


Arkeokast (Archaeology Podcast) began in June 2020.
Its aim -against the backdrop of the COVID-19
pandemic[<sup>(Wikipedia)</sup>][COVID-19], the
restrictions on public gatherings and closed
borders- is to provide an alternative academic
platform for hosting scientific events. 
The podcast's first season was born in the shadow of a
canceled workshop *Moving Seaside* organised by
Néhémie Strupler and Michael Loy. The remimagining of this conference has now 
developed into a longer collaboration.

[COVID-19]: https://en.wikipedia.org/wiki/COVID-19_pandemic

## Contact

You can contact Arkeokast through social media
 or by mailing us at [mail] (to be announced)

[mail]: <mailto:not-set-yet@arkeokast.to-be-announced>

## Logo



{{< figure
src="/images/website/arkeokast_favicon_unframed.svg"
height="300"
alt="Arkeokast Logo"
>}}

The logo represents two variants of the hieroglyphic
sign representing the god Šarruma. It is a pair of
walking legs topped with the sign for god. Nothing to
do with the divine, it represents us walking
and chatting together! [^image_source]

[^image_source]: The logo was redrawn from page 256 of **Sayce,
Archibald Henry (1882)**, "The Monuments of the
Hittites",
*Transactions of the Society of Biblical Archaeology*,
VII, 248-293
https://archive.org/details/transactions07soci/page/256


## License

All the contents of the website are under CC BY-SA 4.0
License (Arkeokast), if not otherwise indicated.
