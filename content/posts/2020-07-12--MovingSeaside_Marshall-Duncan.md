---
title: "Seafaring, Maritimity, and the Neolithisation of Cyprus"
date: 2020-07-12
interviewee: "Duncan Howitt-Marshall"
author: Arkeokast and Duncan Howitt-Marshall
language: en
type: post
draft: true
seasons: "Moving Seaside"
description: ''
featured_image: ''
tags:
  - 'Neolithic'
  - 'Moving Stones'
categories: ''
summary: "An episode on advances in seafaring
technology and the development of a maritime
interaction sphere between communities on the island
and neighbouring mainland"
---

As the Neolithic way of life spread westwards from the
shores of the Levant and Anatolia, seafaring farmers
developed the necessary maritime skills and technology
to transport people, livestock, and new seed crops to
the islands of the Aegean and eastern Mediterranean. In
the case of Cyprus, this process of Neolithisation was
gradual and incremental, following on from earlier
visitations by fisher-foragers at the end of the
Pleistocene. From the late 10th millennium BC onwards,
groups of seafaring foragers and pioneer farmers made
repeated visits to the island, stocking it with wild
game (small pigs) and introducing emmer wheat from the
neighbouring mainland. As seasonal campsites were
established, including the construction of subterranean
storage pits at Ayios Tychonas Klimonas (ca. 9100–8600
Cal BC), a greater emphasis was placed on longer-term
occupations and the introduction of non-endemic species
of animals to hunt and herd, including deer, goat,
sheep, and cattle. This lengthy process of adaptation
to the new island environment transformed the
surrounding sea into a knowable, navigable space, and a
portal for movement and travel.

This paper will argue that advances in seafaring
technology and the development of a maritime
interaction sphere between communities on the island
and neighbouring mainland played a crucial role in
counteracting the potentially “paralysing isolation” of
island life in the early Neolithic. Over time, seafaring
may have assumed an ideological or ritualised context,
especially in regard to the transfer of esoteric
knowledge (the adoption of symbolic practices, rituals,
and social institutions), the maintenance of social
relations (pathways to ancestral homelands?), and the
circulation (and consumption) of exotic materials,
objects, and craft technologies, such as obsidian from
central Anatolia.  For those with the necessary skills
to undertake the challenges and risks of regular
sea-crossings, the benefits must have outweighed the
cost, perhaps by the enhancement of their social status
or prestige within their respective communities of
practice.

Duncan Howitt-Marshall
======================

<div class="pa4 fl">
  <img
      src="../../seasons/moving-seaside/img/portfolio/howitt-marshall_duncan_photo.jpg"
      class="br-100 h5 w1 dip" alt="avatar">
</div>


<!-- {{< figure src="../../seasons/moving-seaside/img/portfolio/howitt-marshall_duncan_photo.jpg">}} -->

Duncan Howitt-Marshall is a maritime archaeologist based in Athens,
Greece. He completed his undergraduate studies in the Department of
Classics and Ancient History (BA Hons.) at the University of Exeter,
followed by an MA in Maritime Archaeology at the University of
Southampton. His main research interests include maritime aspects of
culture in Mediterranean prehistory, the archaeology of islands
(especially Cyprus and Crete), and the origins and early development of
seafaring. He has directed seven seasons of underwater archaeological
survey in Cyprus, and taken part in other maritime and terrestrial field
projects (survey and excavation) in Greece, Cyprus, Sweden and the
United Kingdom. He is in the final write-up stages of a PhD at the
University of Cambridge, and is a long-term Member of the British School
at Athens (BSA).

**Contact:**\
Duncan Howitt-Marshall \
British School at Athens \
Souedias 52 \
10676 Athens - Greece \
<dshowittmarshall@gmail.com>
