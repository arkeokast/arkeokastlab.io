---
title: "Stunting through farming? Rethinking stature, archaeogenetics and nutrition in the Neolithic"
date: 2020-07-16
interviewee: "Dr. Eva Rosenstock"
author: Arkeokast and Eva Rosenstock
language: en
type: post
draft: true
seasons: "Moving Domestication"
tags:
  - 'Neolithic'
  - 'Moving Stones'
categories: ''
summary: "An episode about environment, economies, health and nutrition during the Neolithic Period"
---

Since the 1980ies, the Neolithic transition has been
perceived as a case of impoverished overall health and
nutrition, visible - among other proxies - in
decreasing human adult stature and the assumed
increasing share in carbohydrates in overall calory
intake. However, trends in adult stature as derived
from a Bayesian errors-in-variables model (Groß
2016) applied to the LiVES COstA Digest dataset
      (Rosenstock et al.  2019a) are by no means
uniform and cannot be related to environmental reasons
without further scrutiny. While stature is at best
declining slightly in the Fertile Crescent and Anatolia
and more marked in Southeast Europe (Rosenstock et al.
2019b). Moreover, a similar analysis based on the
δ<sup>15</sup>N and δ<sup>13</sup>C stable isotope dataset collected for
our LiVES group and underlying Scheibner (2016) in
preparation (Rosenstock, Scheibner, Groß, Fernandes in
prep.) shows very different nutritional trends for the
Fertile Crescent, Anatolia, the Aegean and the Balkans.
Apparently, the impact of Neolithization on nutrition,
especially trophic level, and stature was very
different in the Primary vs. the Secondary modes of
Neolithization in Anatolia and its neighbouring
regions. Observable changes in stature during
Neolithization, hence, must be viewed also in the light
of the archaeogenetics of stature (Cox et al. 2019) and
movements of postglacial populations related to
Neolithization and cannot be readily attributed to
changes in nutrition.


Dr. Eva Rosenstock
====================

<div class="pa4 fl">
  <img
      src="../../seasons/moving-domestication/img/portfolio/2-rosenstock_eva_photo.jpg"
      class="br-100 h5 w1 dip" alt="avatar">
</div>

Eva Rosenstock received her doctorate in prehistoric
archaeology from Tübingen University in 2005 and was
holder of the travel grant of the Deutsches
Archäologisches Institut in 2006-2007. The comparative
supra-regional perspective between Near East and Europe
taken in her dissertation on Neolithic tell settlements
has proven a useful approach for many other topics in
Eva’s research with a focus on economic archaeology.
Her projects, based at Freie Universität Berlin where
she has been a research associate since 2006, include
excavations at 6th millennium BC Çatalhöyük West in
Central Anatolia (2006 – 2013, funded by the
Alexander-von-Humboldt-Stiftung), an Emmy Noether
Junior Research Group investigating the development of
human stature in prehistory (2011 – 2019, funded by the
Deutsche Forschungsgemeinschaft) and aspects of cattle
traction in prehistory. Within the framework of the
Einstein Center Chronoi, Eva currently tackles
questions of time reckoning and time management in the
Neolithic.  



**Contact:**

Freie Universität Berlin\
 Einstein Center Chronoi\
 Otto-von-Simson-Straße 7\
 14195 Berlin, Germany\
 <e.rosenstock@fu-berlin.de>

### Bibliography


S. Cox - C. B. Ruff - R. M. Maier -- I. Mathieson, Genetic contributions
to variation in human stature in prehistoric Europe, Proceedings of the
National Academy of Sciences, 116, 2019, 21484.

M. Groß (2016) Modeling body height in prehistory using a
spatio-temporal Bayesian errors-in variables model. Advances in
Statistical Analysis 100/3, 289-311.

E. Rosenstock -- J. Ebert -- R. Martin -- M. Groß, LiVES-COstA Digest:
Checked and updated entries from the LiVES Collection of Osteological
Anthropometry, Edition Topoi, 2019, <https://doi.org/10.17171/2-12-2-1>

E. Rosenstock - J. Ebert - R**. Martin - A. Hicketier - P. Walter - M.
Groß, [Human Stature in the Near East and Europe ca. 10 000 -- 1000 BC:
its spatio-temporal development in a Bayesian errors-in-variables
model]{.underline}. Anthropological and Archaeological Sciences 11(10),
2019, 5657-5690.** <https://doi.org/10.1007/s12520-019-00850-3>

E. Rosenstock, J. Ebert, A. Scheibner, M. Groß, R. Fernandes, Tracing
the food revolutions: a Bayesian approach to human δ15N and δ13C data
from Holocene Southwest Asia and Europe (working title), in prep.

A. Scheibner, Prähistorische Ernährung in Vorderasien und Europa: Eine
kulturgeschichtliche Synthese auf der Basis ausgewählter Quellen.
Studien zum Lebensstandard in der Vorgeschichte, Vol. 1. Rahden: Marie
Leidorf 2016.
