---
title: "Some Methodological Problems About Aegean Connectivity and Interaction Networks"
date: 2020-07-14
interviewee: "Dr. Catherine Perlès"
author: Arkeokast and Catherine Perles
language: en
type: post
draft: true
seasons: "Moving Seaside"
description: ''
featured_image: ''
tags:
  - 'Neolithic'
  - 'Moving Stones'
categories: ''
summary: "An episode about interactions around the
Agean during the Mesolithic and Neolithic periods"
---

Until recently, the development of the Neolithic on
both sides of the Aegean was studied completely
independently and some even denied any relation. The
pendulum has now swung to the opposite extreme, and
terms such as "connectivity", "interrelations",
"interaction networks" are now flourishing in the
recent scientific literature, both for the Mesolithic
and the Neolithic. As a long-term proponent of a
near-eastern origin for the Neolithic of Greece, I am
certainly not willing to deny that relations between
the two areas indeed took place. However, in order to
fully apprehend the nature of these relations, I
believe we need to rely on a rigourous methodological
framework. For instance, Mesolithic seafaring did not
present the same constraints as the transportation of
live animals, and it cannot be simply assumed that
Mesolithic sea routes constituted obvious highways for
the first Neolithic settlers.  Similarly, the mere
presence of Milian obsidian on both sides of the Aegean
does, by itself, demonstrate that the communities that
were using it were interconnected. Finally, the
demonstration of cultural affiliation requires a
distinction between ancestral and derived characters,
and should rely primarily on complex know-hows that
require lineages of apprenticeship and community of
practice.

Dr. Catherine Perlès
====================

<div class="pa4 fl">
  <img
      src="../../seasons/moving-seaside/img/portfolio/perles_catherine_photo.jpeg"
      class="br-100 h5 w1 dip" alt="avatar">
</div>

Catherine Perlès was one of the first to work on the prehistoric flaked
stone assemblages of Greece. This led her to investigate exchange
networks, the origins and organization of Neolithic societies in Greece
and migration processes. Besides a large number of articles, she has
published several volumes on the flaked stones and ornament assemblages
from the Franchthi Cave, and a book on the Early Neolithic in Greece.

**Contact:**\
Dr. Catherine Perlès\
Université Paris Nanterre, CNRS, UMR 7055\
<catherine.perles@cnrs.fr>
