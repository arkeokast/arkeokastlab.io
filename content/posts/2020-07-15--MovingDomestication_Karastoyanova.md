---
title: "Husbandry and hunting during Neolithic period in Bulgaria according to animal remains from archeological sites"
date: 2020-07-15
interviewee: "Dr. Nadezhda Karastoyanova"
author: Arkeokast and Nadezhda Karastoyanova
language: en
type: post
draft: true
seasons: "Moving Domestication"
description: ''
featured_image: ''
tags:
  - 'Neolithic'
  - 'Moving Stones'
categories: ''
summary: "An episode about wild animals, husbandry and dometic mammal in Bulgaria during the Neolithic Period"
---

Neolithic settlements in Bulgaria give a good, but
still incomplete, sample of data showing habitats and
husbandry. There are animal remains of wild and
domestic mammals that are hunted and bred by humans
found in pits, dwellings and other archeological
features in the sites. This gives a general idea of the
management of animal sources (meat and dairy products),
husbandry and livelihood of the people. Such deposits
of animal remains are the main source of data on fauna
and domestication during this period. This gives us
insight into the processes leading to the development
of agriculture during transition between Neolithic and
Chalcolithic period .  This presentation includes the
analysis of  animal remains from three still
unpublished deposits from southern and northern
Bulgaria with more than 26000 analysed bones, horns,
antlers and teeth from large mammals from the orders:
artiodactyla, perissodactyla and carnivora. The
analysis also includes already published data from
sites in the eastern Balkans.

Dr. Nadezhda Karastoyanova
====================

<div class="pa4 fl">
  <img
      src="../../seasons/moving-domestication/img/portfolio/1-nadezhda-karastoyanova-photo.jpg"
      class="br-100 h5 w1 dip" alt="avatar">
</div>

Born March 7th 1984 in Dupnitsa, she has been working
as a zooarcheologist since 2012, and she recently has
been appointed to the National Museum of Natural
History, Sofia, Bulgaria. She has extensive experience
in excavations: 15 years in archaeological site of
different periods and is a specialist in prehistoric
site.  Education: - 2018 — PhD on theme of research:
‘Development of hunt and animal husbandry in Eastern
Balkans: The role of wild and domestic species in
paleoeconomyand changes in natural environment from
Late Neolithic to Late Chalcolithic period’. Advisor:
Prof. Dr Nikolai Spassov; National Museum of Natural
History — BAS, Sofia - 2011 — MA degree in Archaeology,
on theme ‘Late Neolithic faunal remains from a pit
deposit excavated near Sarnevo, Stara Zagora region, in
Southeast Bulgaria’; New Bulgarian University, Sofia -
2009 — Bachelor degree in Archaeology, on theme
‘Archaeological documentation of hill-top sites’; New
Bulgarian University, Sofia



**Contact:**\
Department of “Paleontology and Mineralogy”\
National Museum of Natural History Bulgarian Academy of Science\
1 Tsar Osvoboditel Blvd.\
1000 Sofia, Bulgaria\
Tel: +359 895 606 305

