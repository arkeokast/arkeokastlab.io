---
title: "The Role of Anatolia, Mediterranean Relations During the Neolithic"
date: 2020-07-11
interviewee: "Özlem Aytek"
author: Arkeokast and Özlem Aytek
language: en
type: post
draft: true
seasons: "Moving Seaside"
description: ''
featured_image: ''
tags:
  - 'Neolithic'
  - 'Moving Stones'
categories: ''
summary: "Recent data concerning coastal and
maritime expansion of the components of the Neolithic
package from Anatolian littoral to other parts of the
Mediterranean"
---

The Mediterranean Basin had played a critical role in
the emergence and the establishment of cultural
contacts among Asia, Africa and Europe.  Along the
Mediterranean there are at least one thousand habitable
islands and island archipelagos that enable all sorts
of cultural contact, including expansion,
acculturation, adaptation or transfer of know-how.
Anatolia has a long-strand of the coastline along the
Mediterranean and being the home of primary
neolithization is of a critical role for understanding
the modalities of the dispersal of early sedentary
farming throughout the Mediterranean Basin. Sedentary
life began around 10.000 BC in the Near East and
developed until approximately 7.400 BC in the region,
before being dispersed to other regions; evidently both
maritime and land routes along the coast played a prime
role in the westward expansion of Neolithic elements.
It is also evident that the expansion of the Neolithic
way of life took place through a diversity of
interactions. The present paper will provide a
conspectus on recent data concerning coastal and
maritime expansion of the components of the Neolithic
package from Anatolian littoral to other parts of the
Mediterranean.

Özlem Aytek
===========

<div class="pa4 fl">
  <img
      src="../../seasons/moving-seaside/img/portfolio/aytek_ozlem_photo.jpg"
      class="br-100 h5 dip" alt="avatar">
</div>

Özlem Aytek has been a lecturer in Archaeology at the University of
Pamukkale (Denizli-Turkey) since 2008, and is a PhD candidate in
Prehistory at the University of İstanbul. Her main work to date has been
on the Neolithic Pottery of the Near East; currently, her work focuses
on Neolithic of Mediterranean. She has experience in various excavations
and is currently a team member of the excavation at Aşağı Pınar and
Yumuktepe.

**Contact:**\
Özlem Aytek\
Pamukkale University\
Department of Archaeology\
20070 Denizli\
Turkey\
<ozlemaytek@yahoo.com>
