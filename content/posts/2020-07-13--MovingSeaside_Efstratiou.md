---
title: "First Farmers on Cyprus - Shaping the Brave ‘New World’: Archaeological Stories of Transition"
date: 2020-07-13
interviewee: "Dr. Nikos Efstratiou"
author: Arkeokast and Nikos Efstratiou
language: en
type: post
draft: true
seasons: "Moving Seaside"
description: ''
featured_image: ''
tags:
  - 'Neolithic'
  - 'Moving Stones'
categories: ''
summary: "The long transformation process towards
farming on Cyprus or how the early farming niche of the
mainland was transferred to the island"
---

Recent archaeological investigations in Cyprus have
considerably changed our perception of the arrival of
early farmers on the island. Evidence from the
emblematic small rock shelter of Akrotiri-*Aetokremnos*
(10.500 BC) and a number of less well-documented
Epipalaeolithic sites on the island provide a glimpse
of the hunter-gatherer way of life in Cyprus while the
case of the Pre-Pottery Neolithic village of Ag.
Tychonas-*Klimonas* (9.100-8.600 BC) shows, albeit
tentatively, that early cultivated species (emmer
wheat) was introduced to Cyprus from the mainland. At
the same time hunting (small wild boar) and gathering
of plants (pistachio) continued to support a foraging
niche, entertaining the presence of a rather obscure
archaeologically transition period to farming on the
island. The conditions under which foraging on Cyprus
became unstable and therefore open to a long
transformation process towards farming or how the early
farming niche of the mainland was transferred to the
island, will be examined on the context of the new
available archaeological evidence from the mainland.
Moreover, the paper will probe the limits of concepts
such as 'insularity', 'sea foraging' and 'maritime way
of life' and their role in shaping the early
archaeological record and therefore the prevailing
narratives. This will include the archaeological
evidence from the newly excavated Aceramic Khirokitia
culture site of Ag. Ioannis/Vretsia-*Upper Roudias*
(mid-7^th^ mill BC) in upland Troodos, which comments
on issues of 'insularity' and 'regionality' in early
Cyprus. The degree of interaction or absence of it
between the foraging and farming way of life on the
island, which often reveal territorial preferences,
networks of contacts, exchanges and procurement
strategies etc, will be presented and discussed.

Dr. Nikos Efstratiou
====================

<div class="pa4 fl">
  <img
      src="../../seasons/moving-seaside/img/portfolio/efstratiou_nikos_photo.jpg"
      class="br-100 h5 w1 dip" alt="avatar">
</div>

Born in Australia, Nikos Efstratiou studied archaeology
(B.A) at the University of Thessaloniki, Greece and
prehistory at the Institute of Archaeology, University
of London, UK (M.A, Ph.D). He has directed
interdisciplinary archaeological and
ethnoarchaeological field projects (excavations,
surveys) in various parts of Greece (Northern Sporades
Aegean Islands, Thrace, Western Macedonia, Crete,
Lemnos) and abroad (Spain, Cyprus, Sultanate of Oman).
He is currently teaching courses of Prehistoric
Archaeology and Ethnoarchaeology at the Department of
Archaeology.

**Contact:**\
Dr. Nikos Efstratiou\
Professor of Prehistoric Archaeology\
Department of Archaeology\
Aristotle University of Thessaloniki 54 124\
Greece\
<efstrati@hist.auth.gr>
