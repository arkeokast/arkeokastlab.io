---
title: "Seafarers and Farming Pioneers Around 9000 Years Ago"
date: 2020-07-10
interviewee: "Prof. Dr. Barbara Horejs"
author: Arkeokast and Barbara Horejs
language: en
type: post
draft: true
seasons: "Moving Seaside"
description: ''
featured_image: ''
tags:
  - 'Neolithic'
  - 'Moving Stones'
categories: ''
summary: "An overview of our current knowledge about
the transformation towards the Neolithic in western Anatolia"
---

This contribution provides an overview of our current
knowledge about the transformation towards the
Neolithic in western Anatolia and offers a narrative
for their interpretation. New cultural and symbolic
practices, economies, and technologies in the seventh
millennium appear as the paradox of a short revolution
embedded in a long-term process of interaction,
knowledge-transfer and adaptation. The regions in focus
of this contribution are the Aegean islands and the
western Anatolian coast in the early to mid Holocene
between 9th and 7th millennia BCE.  Long-term field
investigations and multi-disciplinary material studies
offer new primary data for discussing the formation of
Neolithic house-based societies around 6700 calBC. The
excavations at Çukuriçi Höyük, a former coastal side in
the Izmir region, revealed a bundle of
interdisciplinary data leading to the already published
model of maritime colonization by pioneers coming to
the central coast of western Anatolia via the
Mediterranean. These Neolithic pioneers show crucial
economic and social aspects in common that belong to
the first Neolithic lifestyle in the region, and stand
in strong contrast to the earlier Aegean Mesolithic.
These new Neolithic aspects include a bundle of
innovations − the 'Neolithic package' − that is related
to a broader package of skills and knowledge affecting
all crucial aspects of individual and community life.
Seafaring mobile groups appear to play an important
role in the transfer of the nautical knowledge
established over many generations to the incoming
Neolithic pioneers. This nautical package did not only
include seafaring skills, but also the knowledge of
routes to important sources on the Aegean Islands, such
as jadeite or obsidian. While the use and exchange of
obsidian is a well-known topic, the recent discovery of
the jadeite source on the Syros Island, its procurement
in the 7th millennium BCE and use by Çukuriçi early
farmers is highly promising to offer new insights into
maritime networks. The impact of the Mesolithic
seafaring networks on the Neolithisation process of the
regions in focus remains as an open question and will
be discussed in the perspective of these new data.

Prof. Dr. Barbara Horejs
========================


<div class="pa4 fl">
  <img
      src="../../seasons/moving-seaside/img/portfolio/horejs_barbara_photo.jpg"
      class="br-100 h5 w1 dip" alt="avatar">
</div>


Barbara Horejs has been director of the Institute of Oriental and
European Archaeology of the Austrian Academy of Sciences since 2013, and
is also professor for Prehistory at Tübingen University. She studied
Prehistory at the University of Vienna, Athens and at the Free
University Berlin. Having graduated from the Free University Berlin in
2005, she was awarded with a START prize from the Austrian Science fund,
as well as a Starting Grant from the European Research Council (ERC).
Her fieldwork covers Turkey, Greece, the Balkans, while her research
focuses on prehistoric archaeology, world archaeology and the
developments in human history in far-reaching associations during the
Neolithic, Chalcolithic and Bronze ages. Prof. Horejs has previously
taught at the Universities of Berlin, Bratislava, Tübingen and Vienna,
and she is the author of several books focusing on the Neolithic and
subsequent period.

**Contact:**\
Prof. Dr. Barbara Horejs\
Director OREA\
Institute for Oriental and European Archaeology\
Austrian Academy of Sciences\
Hollandstrasse 11-13, A-1020 Vienna

