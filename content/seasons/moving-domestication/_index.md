---
title: "Moving Domestication"
description: "Changes in faunal and floral economies in the Neolithic"
date: 2020-06-25T13:49:59+03:00
draft: false
author: Arkeokast
type: season
summary: "A season on the transition from foraging to
farming and hunting to herding in the Neolithic period"
---

<div class="flex flex-wrap justify-center tj">

 <div class="w-80 pa3">
{{< figure src="./img/ab_tr_en_color.png"
alt="EU-TR Banner"
caption= "A project co-financed by the European Union and the Republic of Turkey"
>}}
 </div>

</div>

### Moving Domestication

This season focuses on the transition from foraging to
farming and hunting to herding as a prominent threshold
in human history. Economies based on domesticated
plants and animals are associated with radical
reorganisation of human societies as well as major
manipulation in biodiversity and landscape. Aim of the
season is be to bring together new sources of
information to consider, to stimulate academic
discussion on the origins, spreading, and changes of
domesticated animal and plant profiles in the west
Asian and southeastern European regions, and outlining
our current understanding of these developments. This
discussion is part of the international documentary
film "Moving Stones" which is part of the larger
project "*Europe's Neolithic Bridge: Documenting and
Disseminating the Neolithic Heritage of Anatolia*",
funded by the Grant Scheme for EU-Turkey Intercultural
Dialogue (ICD).

Increasingly more sophisticated methods for documenting
domestication in plants and animals have allowed for
better understanding of the complex interactions that
occurred amongst humans, animals, and their
environments during the Neolithic. This season focus on
faunal and floral economies in the North Mesopotamian,
Anatolian and the Balkan regions of the Neolithic
period. We aim to discuss the multilayered and complex
processes regarding the spread of domesticated plants
and livestock and the timeframe over which this spread
took place.  While an increased connectivity between
South-east Anatolian and the Balkans was already
established by the start of the Neolithic period, the
circulation of raw materials, tools, skills, and even
material styles throughout the region before and during
the Neolithic established significant cultural
corridors of communication. The aim of this workshop is
to explore the role of faunal and floral resources
within these interactions.

### Europe's Neolithic Bridge

This project aims to highlight the Anatolian
archaeological heritage by means of a documentary film
and a series of related activities. The documentary
film **Moving Stones** will illustrate the adoption and
transfer from the Near East to Europe of some of the
most significant changes of the Human lifestyle: the
domestication of plants and animals as well as
sedentism. By the actions of this project, the
Anatolian archaeological heritage will be put forward
as the proof of the continuous ongoing dialogue,
collaboration and cross-border exchanges of all
periods. It is funded by the Grant Scheme for EU-Turkey
Intercultural Dialogue Programme (ICD), which is
co-financed by the European Union and republic of
Turkey and implemented by the [Yunus Emre
Institute](https://icd.yee.org.tr/en/).


### Moving Stones

{{< figure src="./img/moving_stones_banner.jpg"
height="300"
alt="Banner of the documentary film Moving Stones"
>}}

The documentary film **Moving Stones** (directed by
Nalân and Enis Sakızlı, VTR YAPIM) will show how
innovations characteristic of the Neolithic period
moved from Western Asia to Southern Europe, via
Anatolia. The documentary film project is being carried
on with a core team of 8 people. During the filming of
the documentary, 35 Neolithic sites and 22 museums have
been shot on location. Interviews of a total of 43
scientists both from Turkey and abroad had been filmed.
Also, 15 students and 23 people, who are members of the
local community had been filmed. It is set for release
in July 2020. The workshop in Athens will help the film
makers to shape the academic content of the documentary
in line with the most recent research on the Neolithic
Mediterranean. Clips from session discussions will
feature in the final film.

### Visit the project

<https://neolithic-bridge.org>

Archaeological Unit\
French Institute for Anatolian Studies\
Nuru Ziya Sk No:10, Beyoğlu Merkez\
34433 Istanbul - Turkey



<div class="flex flex-wrap justify-center">
 <div class="w-20 pa1">
  <a href="https://cfcu.gov.tr" target="_blank" rel="noopener noreferrer">
   <img src="./img/logo_cfcu.png" />
  </a>
</div>
 <div class="w-20 pa1">
<a href="https://ifea-istanbul.net" target="_blank" rel="noopener noreferrer">
<img src="./img/logo_ifea.jpg" />
</a>
  </div>
 <div class="w-20 pa1">
<a href="https://dernekimece.com/" target="_blank" rel="noopener noreferrer">
<img src="./img/logo_imece.png" />
</a>
  </div>
 <div class="w-30 pa1">
<a href="https://twitter.com/BabilSTK" target="_blank" rel="noopener noreferrer">
<img  src="./img/logo_babil.jpg" />
</a>
  </div>
 <div class="w-40 pa1">
  <a href="https://icd.yee.org.tr" target="_blank" rel="noopener noreferrer">
<img src="./img/logo_icd.png" />
</a>
  </div>
 <div class="w-20 pa1">
<a href="https://yee.org.tr" target="_blank" rel="noopener noreferrer">
<img  src="./img/logo_ye.png" />
</a>
  </div>
</div>









<div class="f7 pa4">
This season was created and maintained with the
financial support of the European Union.

Its contents are the sole responsibility of IFEA and do
not necessarily reflect the views of the European Union
or Yunus Emre Institute
</div>


# Episodes
