---
title: "Seasons"
date: 2020-06-25T13:49:59+03:00
draft: false
featured_image: 'images/website/2019-06-11_Mount_Kazbek.jpg'
cover_dimming_class: "bg-black-10"
---

Podcast episodes are arranged by seasons, focussing
on a topic, an event or a particular theme.

