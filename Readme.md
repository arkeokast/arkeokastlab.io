[![Build Status](https://gitlab.com/arkeokast/arkeokast.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/arkeokast/arkeokast.gitlab.io/-/commits/master)

# How to generate this site

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. [Install][] [Hugo][]

1. Clone this project

    `git clone --recurse-submodules https://gitlab.com/arkeokast/arkeokast.gitlab.io.git arkeokast_website`

    `cd arkeokast_website`

1. Generate the website

    `hugo serve`

1. Your site can be accessed under `localhost:1313/hugo/`.

[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/

# How it works?

## GitLab CI

This project's static Pages are built by GitLab  [GitLab CI][ci],
following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

[ci]: https://about.gitlab.com/gitlab-ci/

## Modifications to text

### Change text content

To modify the text, most of the content is in Markdown
format under content. The file "\_index.md" in folder
and subfolder represent the list (of posts, seasons,
...).

### Change layout

If new to Hugo, preferred method is to open an issue
with improvement proposal.
